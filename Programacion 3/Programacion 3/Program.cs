﻿using System;
using System.Threading;
using Microsoft.VisualBasic;
using System.IO;
using System.Net;

using Newtonsoft.Json.Linq;

namespace Programacion_3
{
    public static class GlobalVariables
    {
        public const char playerChar = '☺';//PLAYER character
        public const char mineChar = 'X';//MINES character
        public const char pointsChar = '+';//POINTS character
        public const char enemiesChar = '*';
        public const char bulletChar = 'o'; //BULLET character.

        public static bool fireBullet = false; //If the Bullet is on or off.

        public static int bulletX = 0, bulletY = 0; //Bullet Position
        public static int playerX = 0, playerY = 0;//Player position

        public const int minesNumber = 8;//AMOUNT OF MINES. Change from here, nowhere else
        public const int pointsNumber = 8;//AMOUNT OF POINTS ON SCREEN. Change from here, nowhere else
        public const int enemiesNumber = 2;//AMOUNT OF ENEMIES ON SCREEN. Change from here, nowhere else      

        public static int[,] minePosition = new int[minesNumber, 2];//Do not tocuh
        public static int[,] pointPosition = new int[pointsNumber, 2];//Do not touch
        public static int[,] enemyPosition = new int[enemiesNumber, 2];//Do not touch
        public static string WelcomeMSG = null;//Do not touch

        public static int life = 3; //PLAYER LIVES
        public static int bulletDirection = 0; //Bullet stuff

        public const string fileWelcome = "WelcomeText.txt";//Change the WelcomeMSG file location from here
        public const string fileResume = "PlayerPos.binary";//Change the Resume file location from here
        public const string fileHighscore = "HighScore.dat";//Change the HighScore file location from here

        public const string city = "BuenosAires";//NO SPACE

        public static int pointsScored = 0;//Points scored in this round
        public static int highscore = 0;//Highest recorded points

        public static bool enemiesOn = false;//When true, enemies spawn and move    
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            //////////
            //START//
            /////////
                       
            Console.OutputEncoding = System.Text.Encoding.Unicode;//Enable Unicode to access more characters. Especifically ♥ for the lives

            //WEB QUERY
            string toSearch = "https://query.yahooapis.com/v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"+GlobalVariables.city+"%2C%20tx%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
            WebRequest req = WebRequest.Create(toSearch.ToLower());//cast as lower case string for API purposes            

            try
            {
                WebResponse respuesta = req.GetResponse();

                Stream stream = respuesta.GetResponseStream();
                StreamReader sr = new StreamReader(stream);

                JObject data = JObject.Parse(sr.ReadToEnd());

                string response = (string)data["count"];

                if (response=="0")//White if couldnt find the city
                {
                    Console.BackgroundColor = ConsoleColor.White;
                }
                else//Yellow for Sunny, Gray for Cloudy and DarkGreen for other
                {
                    string clima = (string)data["query"]["results"]["channel"]["item"]["condition"]["text"];
                    if (clima!=null)
                    {
                        if (clima == "Sunny")
                        {
                            Console.BackgroundColor = ConsoleColor.Yellow;
                        }
                        else if (clima == "Cloudy")
                        {
                            Console.BackgroundColor = ConsoleColor.Gray;
                        }
                        else
                        {
                            Console.BackgroundColor = ConsoleColor.DarkGreen;
                        }
                    }
                    else
                    {
                        //In case of problems
                        Console.BackgroundColor = ConsoleColor.DarkBlue;
                    }
                   
                }               
            }

            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.NameResolutionFailure)//If the site is down
                {
                    Console.Write("No se pudo conectar. Intente despues...");
                }
                else
                {
                    Console.Write("Error");
                }
            }

            //VISUALS
            Console.ForegroundColor = ConsoleColor.Red;
            Console.CursorVisible = false;

            //MENU
            Console.WriteLine("Press Esc to exit, or any other key to start");
            if (Console.ReadKey().Key==ConsoleKey.Escape)
            {
                Environment.Exit(0);
            }

            //START MESSAGE
            WelcomeMsg();

            Resume(Console.ReadKey().Key); //Resume from possible previous session

            //PLAYER            
            UpdatePlayerPos(GlobalVariables.playerChar, GlobalVariables.playerX, GlobalVariables.playerY); //Start in the middle of the screen

            //MINES
            CreateMines(GlobalVariables.minesNumber);//Creates a set amount of mines in a random position of the screen

            //POINTS
            CreatePoints(GlobalVariables.pointsNumber);//Creates a set amount of points in a random position of the screen

            //////////////
            //GAME LOOP//
            /////////////

            while (true)
            {
                
                if (Console.KeyAvailable)//If I pressed anything
                {
                    var keyRead = Console.ReadKey().Key;

                    //PLAYER MOVEMENT
                    switch (keyRead)
                    {
                        case ConsoleKey.DownArrow:
                            if (GlobalVariables.playerY < Console.WindowHeight)//As long as it doesnt go over the border, it's fine. At least for now
                            {
                                GlobalVariables.playerY++;
                                GlobalVariables.bulletDirection = 0;
                            }
                            break;
                        case ConsoleKey.UpArrow:
                            if (GlobalVariables.playerY > 0)//It's basically the same as doing (Console.WindowHeight)-(Console.WindowHeight), which will always be 0
                            {
                                GlobalVariables.playerY--;
                                GlobalVariables.bulletDirection = 1;
                            }
                            break;
                        case ConsoleKey.LeftArrow:
                            if (GlobalVariables.playerX > 0)//Same as before
                            {
                                GlobalVariables.playerX--;
                                GlobalVariables.bulletDirection = 2;
                            }
                            break;
                        case ConsoleKey.RightArrow:
                            if (GlobalVariables.playerX < Console.WindowWidth - 1)//It's -1 so it doesnt overflow. No idea why it only happens with the right side
                            {
                                GlobalVariables.playerX++;
                                GlobalVariables.bulletDirection = 3;
                            }
                            break;
                        case ConsoleKey.Spacebar:
                            if (GlobalVariables.fireBullet == false)
                            {
                                GlobalVariables.bulletX = GlobalVariables.playerX;
                                GlobalVariables.bulletY = GlobalVariables.playerY;
                                GlobalVariables.fireBullet = true;
                            }                 
                            break;
                        case ConsoleKey.Escape://Escape
                            Resume(keyRead);
                            break;
                    }
                    UpdatePlayerPos(GlobalVariables.playerChar, GlobalVariables.playerX, GlobalVariables.playerY);//Update position of player
                }
                else
                {
                    Thread.Sleep(100);//This is so the player doesnt update very frame unless a key is pressed
                }

                //UPDATE
                UpdateBulletPos(GlobalVariables.bulletChar);
                UpdateMines(GlobalVariables.minePosition);
                UpdatePoints(GlobalVariables.pointPosition);
                for (int i = 0; i < GlobalVariables.life; i++)
                {
                    Console.SetCursorPosition(0 + i, 0);
                    Console.Write("♥");
                }
                Console.SetCursorPosition(0, 1);
                Console.WriteLine("Points: "+GlobalVariables.pointsScored);
                Console.SetCursorPosition(0, 2);
                HighScore(GlobalVariables.pointsScored);
                Console.WriteLine("HighScore: "+GlobalVariables.highscore);
                Enemies(GlobalVariables.enemiesNumber);
                GameOver();
            }
        }

        public static void Enemies(int numberOfEnemies)
        {
            Random random = new Random();
            if (GlobalVariables.enemiesOn==false)
            {                
                for (int i = 0; i < numberOfEnemies; i++)
                {
                    do
                    {
                        Console.SetCursorPosition(random.Next(1, Console.WindowWidth - 2), random.Next(1, Console.WindowHeight - 1));
                    } while (Console.CursorLeft == GlobalVariables.playerX || Console.CursorTop == GlobalVariables.playerY || Console.CursorLeft == GlobalVariables.pointPosition[i, 0] || Console.CursorLeft == GlobalVariables.pointPosition[i, 1]);//An enemy CANNOT spawn on top of the player or a point

                    GlobalVariables.enemyPosition.SetValue(Console.CursorLeft, i, 0);
                    GlobalVariables.enemyPosition.SetValue(Console.CursorTop, i, 1);//Save the array with the position of the mine on an array which contains all the enemies

                    Console.Write(GlobalVariables.enemiesChar);
                }
                GlobalVariables.enemiesOn = true;
            }
            else
            {                
                for (int i = 0; i < GlobalVariables.enemiesNumber; i++)
                {
                    Console.SetCursorPosition(GlobalVariables.enemyPosition[i, 0], GlobalVariables.enemyPosition[i, 1]);
                    Console.Write(GlobalVariables.enemiesChar);
                    int symbol = random.Next(0, 1);
                    if (symbol==0)
                    {
                        if (GlobalVariables.enemyPosition[i, 0]!=Console.WindowWidth-1 && GlobalVariables.enemyPosition[i, 1]!=Console.WindowHeight)
                        {
                            GlobalVariables.enemyPosition.SetValue(GlobalVariables.enemyPosition[i, 0]++, i, 0);
                            GlobalVariables.enemyPosition.SetValue(GlobalVariables.enemyPosition[i, 1]++, i, 1);
                        }
                                           
                    }
                    else if(symbol==1)
                    {
                        if (GlobalVariables.enemyPosition[i, 0] != Console.WindowWidth - 1 && GlobalVariables.enemyPosition[i, 1] != Console.WindowHeight)
                        {
                            GlobalVariables.enemyPosition.SetValue(GlobalVariables.enemyPosition[i, 0]--, i, 0);
                            GlobalVariables.enemyPosition.SetValue(GlobalVariables.enemyPosition[i, 1]--, i, 1);
                        }
                    }

                    if (GlobalVariables.playerX == GlobalVariables.enemyPosition[i, 0] && GlobalVariables.playerY==GlobalVariables.enemyPosition[i,1])
                    {
                        GlobalVariables.playerX = (Console.WindowWidth / 2) - 1;
                        GlobalVariables.playerY = Console.WindowHeight / 2; //Center of the screen
                        GlobalVariables.life--;
                    }

                }                
            }
        }

        //WELCOME MESSAGE. USE AT START
        public static void WelcomeMsg()
        {
            if (GlobalVariables.WelcomeMSG == null)
            {
                Console.Clear();
                if (File.Exists(GlobalVariables.fileWelcome))
                {
                    FileStream fileRead = File.OpenRead(GlobalVariables.fileWelcome);
                    StreamReader TXTReader = new StreamReader(fileRead);
                    GlobalVariables.WelcomeMSG = TXTReader.ReadLine();
                    TXTReader.Close();
                    fileRead.Close();
                    Console.WriteLine(GlobalVariables.WelcomeMSG);
                    Thread.Sleep(2000);
                    Console.Clear();

                }
                else
                {
                    GlobalVariables.WelcomeMSG = Interaction.InputBox("Please insert Welcome Message", "File does not exist or is corrupt", "Default", 0, 0);
                    FileStream fileWrite = File.Create(GlobalVariables.fileWelcome);
                    StreamWriter TXTWriter = new StreamWriter(fileWrite);

                    TXTWriter.WriteLine(GlobalVariables.WelcomeMSG);
                    TXTWriter.Close();
                    fileWrite.Close();
                    Console.WriteLine(GlobalVariables.WelcomeMSG);
                    Thread.Sleep(2000);
                    Console.Clear();
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine(GlobalVariables.WelcomeMSG);
                Thread.Sleep(2000);
                Console.Clear();
            }
        }

        //BULLET UPDATE. USE AT UPDATE
        public static void UpdateBulletPos(char bullet)
        {
            if (GlobalVariables.fireBullet == true)
            {
                switch (GlobalVariables.bulletDirection)
                {
                    case 0:

                        GlobalVariables.bulletY++;
                        Console.Clear();
                        Console.SetCursorPosition(GlobalVariables.bulletX, GlobalVariables.bulletY);
                        Console.Write(bullet);
                        break;
                    case 1:
                        GlobalVariables.bulletY--;
                        Console.Clear();
                        Console.SetCursorPosition(GlobalVariables.bulletX, GlobalVariables.bulletY);
                        Console.Write(bullet);
                        break;
                    case 2:
                        GlobalVariables.bulletX++;
                        Console.Clear();
                        Console.SetCursorPosition(GlobalVariables.bulletX, GlobalVariables.bulletY);
                        Console.Write(bullet);
                        break;
                    case 3:
                        GlobalVariables.bulletX--;
                        Console.Clear();
                        Console.SetCursorPosition(GlobalVariables.bulletX, GlobalVariables.bulletY);
                        Console.Write(bullet);
                        break;
                }
            }
            if (GlobalVariables.bulletX <= 0 || GlobalVariables.bulletX >= Console.WindowWidth || 
                GlobalVariables.bulletY <= 0 || GlobalVariables.bulletY >= Console.WindowHeight)
            {
                GlobalVariables.fireBullet = false;
            }
            
                
                
        }

        //PLAYER POSITION UPDATE. USE AT UPDATE
        public static void UpdatePlayerPos(char player, int playerX = 0, int playerY = 0)
        {
            try
            {
                if (playerX >= 0 && playerY >= 0) // 0-based
                {
                    Console.Clear();
                    Console.SetCursorPosition(playerX, playerY);
                    Console.Write(player);
                }
            }
            catch (Exception)
            {
            }
        }

        //MINE GENERATOR. USE AT START
        public static void CreateMines(int numberOfMines)
        {
            Random random = new Random();

            for (int i = 0; i < numberOfMines; i++)
            {
                do
                {
                    Console.SetCursorPosition(random.Next(1, Console.WindowWidth - 2), random.Next(1, Console.WindowHeight - 1));
                } while (Console.CursorLeft == GlobalVariables.playerX || Console.CursorTop == GlobalVariables.playerY || Console.CursorLeft == GlobalVariables.pointPosition[i, 0] || Console.CursorLeft == GlobalVariables.pointPosition[i, 1]);//A mine CANNOT spawn on top of the player or a point

                GlobalVariables.minePosition.SetValue(Console.CursorLeft, i, 0);
                GlobalVariables.minePosition.SetValue(Console.CursorTop, i, 1);//Save the array with the position of the mine on an array which contains all the mines

                Console.Write(GlobalVariables.mineChar);
            }
        }

        //MINE UPDATE. USE AT UPDATE
        public static void UpdateMines(int[,] mineArray)
        {
            for (int i = 0; i < GlobalVariables.minesNumber; i++)
            {
                Console.SetCursorPosition(mineArray[i, 0], mineArray[i, 1]);
                Console.Write(GlobalVariables.mineChar);              
            }
        }

        //POINTS GENERATOR. USE AT START
        public static void CreatePoints(int numberOfPoints)
        {
            Random random = new Random();

            for (int i = 0; i < numberOfPoints; i++)
            {
                do
                {
                    Console.SetCursorPosition(random.Next(1, Console.WindowWidth - 2), random.Next(1, Console.WindowHeight - 1));
                } while (Console.CursorLeft == GlobalVariables.playerX || Console.CursorTop == GlobalVariables.playerY || Console.CursorLeft == GlobalVariables.minePosition[i, 0] || Console.CursorLeft == GlobalVariables.minePosition[i,1]);//A point CANNOT spawn on top of the player or a mine

                GlobalVariables.pointPosition.SetValue(Console.CursorLeft, i, 0);
                GlobalVariables.pointPosition.SetValue(Console.CursorTop, i, 1);//Save the array with the position of the point on an array which contains all the points

                Console.Write(GlobalVariables.pointsChar);
            }
        }

        //MINE UPDATE. USE AT UPDATE
        public static void UpdatePoints(int[,] pointArray)
        {
            Random random = new Random();
            for (int i = 0; i < GlobalVariables.pointsNumber; i++)
            {
                if (GlobalVariables.playerX == pointArray[i,0] && GlobalVariables.playerY == pointArray[i,1])
                {
                    GlobalVariables.pointsScored++;
                    do
                    {
                        Console.SetCursorPosition(random.Next(1, Console.WindowWidth - 2), random.Next(1, Console.WindowHeight - 1));
                    } while (Console.CursorLeft == GlobalVariables.playerX || Console.CursorTop == GlobalVariables.playerY || Console.CursorLeft == GlobalVariables.minePosition[i,0] || Console.CursorLeft == GlobalVariables.minePosition[i,1]);//A point CANNOT spawn on top of the player or a mine
                    GlobalVariables.pointPosition.SetValue(Console.CursorLeft, i, 0);
                    GlobalVariables.pointPosition.SetValue(Console.CursorTop, i, 1);//Save the array with the position of the point on an array which contains all the points
                }
                Console.SetCursorPosition(pointArray[i, 0], pointArray[i, 1]);
                Console.Write(GlobalVariables.pointsChar);
            }
        }

        //GAME OVER. USE AT UPDATE
        public static void GameOver()
        {
            
            //MINES GAME OVER
            for (int i = 0; i < GlobalVariables.minesNumber; i++)
            { 
                if (GlobalVariables.playerX == GlobalVariables.minePosition[i, 0] && GlobalVariables.playerY == GlobalVariables.minePosition[i, 1])
                {
                    GlobalVariables.playerX = (Console.WindowWidth / 2) - 1;
                    GlobalVariables.playerY = Console.WindowHeight / 2; //Center of the screen
                    GlobalVariables.life--;
                }
            }
            if (GlobalVariables.life <= 0)
            {
                GlobalVariables.highscore = GlobalVariables.pointsScored;
                Console.Clear();
                string gameOver = "GAME OVER";
                Console.SetCursorPosition((Console.WindowWidth / 2) - gameOver.Length / 2, Console.WindowHeight / 2);
                Console.Write(gameOver);
                string options = "PRESS R TO RESTART OR ESC TO EXIT";
                Console.SetCursorPosition((Console.WindowWidth / 2) - options.Length / 2, (Console.WindowHeight / 2) + 1);
                Console.Write(options);
                GameOverScreen();
            }

        }

        //DO NOT USE. FOR USE OF GameOver FUNCTION ONLY
        protected static void GameOverScreen()
        {
            var keyRead = Console.ReadKey().Key;

            switch (keyRead)
            {
                case ConsoleKey.R://Restart game
                    Console.Clear();
                    GlobalVariables.playerX = Console.WindowWidth / 2;
                    GlobalVariables.playerY = Console.WindowHeight / 2;
                    GlobalVariables.life = 3;
                    for (int j = 0; j < GlobalVariables.minesNumber; j++)
                    {
                        GlobalVariables.minePosition[j, 0] = 0;
                        GlobalVariables.minePosition[j, 1] = 0;
                    }
                    for (int i = 0; i < GlobalVariables.pointsNumber; i++)
                    {
                        GlobalVariables.pointPosition[i, 0] = 0;
                        GlobalVariables.pointPosition[i, 0] = 0;
                    }
                    CreateMines(GlobalVariables.minesNumber);
                    CreatePoints(GlobalVariables.pointsNumber);
                    GlobalVariables.pointsScored = 0;
                    Console.Clear();
                    break;

                case ConsoleKey.Escape://End game
                    HighScore(GlobalVariables.pointsScored);
                    Environment.Exit(0);
                    break;
            }
        }

        public static void HighScore(int score)
        {
            GlobalVariables.highscore = score;

            if (File.Exists(GlobalVariables.fileHighscore))
            {
                FileStream fileRead = File.OpenRead(GlobalVariables.fileHighscore);
                StreamReader Reader = new StreamReader(fileRead);
                fileRead.Position = 0;//Start from the beginning
                int storedScore = 0;
                storedScore = Int32.Parse(Reader.ReadLine());
                Reader.Close();
                fileRead.Close();

                if (GlobalVariables.highscore>=storedScore)
                {
                    FileStream fileWrite = File.OpenWrite(GlobalVariables.fileHighscore);
                    StreamWriter Writer = new StreamWriter(fileWrite);
                    fileWrite.Position = 0;
                    Writer.WriteLine(GlobalVariables.highscore);
                    Writer.Close();
                    fileWrite.Close();
                }
                else
                {
                    GlobalVariables.highscore = storedScore;
                }
               
            }
            else
            {
                FileStream fileWrite = File.Create(GlobalVariables.fileHighscore);
                StreamWriter Writer = new StreamWriter(fileWrite);
                fileWrite.Position = 0;
                Writer.WriteLine(GlobalVariables.highscore);
                Writer.Close();
                fileWrite.Close();
            }
        }

        //QUIT AND SAVE. USE AT START AND AT ESCAPE UPDATE
        public static void Resume(ConsoleKey key)
        {
            if (key==ConsoleKey.Escape)
            {
                HighScore(GlobalVariables.pointsScored);
                if (File.Exists(GlobalVariables.fileResume))
                {
                    FileStream fileRead = File.OpenWrite(GlobalVariables.fileResume);
                    StreamWriter Writer = new StreamWriter(fileRead);
                    fileRead.Position = 0;//Start from the beginning
                    Writer.WriteLine(GlobalVariables.playerX);
                    Writer.WriteLine(GlobalVariables.playerY);
                    Writer.Close();
                    fileRead.Close();
                    Environment.Exit(0);
                }
                else
                {
                    FileStream fileWrite = File.Create(GlobalVariables.fileResume);
                    StreamWriter Writer = new StreamWriter(fileWrite);
                    fileWrite.Position = 0;
                    Writer.WriteLine(GlobalVariables.playerX);
                    Writer.WriteLine(GlobalVariables.playerY);
                    Writer.Close();
                    fileWrite.Close();
                    Environment.Exit(0);
                }
            }

            else
            {
                if (File.Exists(GlobalVariables.fileResume))
                {
                    FileStream fileRead = File.OpenRead(GlobalVariables.fileResume);
                    StreamReader Reader = new StreamReader(fileRead);
                    fileRead.Position = 0;
                    var testX = Reader.ReadLine();
                    var testY = Reader.ReadLine();
                    GlobalVariables.playerX = Int32.Parse(testX);
                    GlobalVariables.playerY = Int32.Parse(testY);
                    Reader.Close();
                    fileRead.Close();
                }
                else
                {
                    GlobalVariables.playerX = (Console.WindowWidth / 2) - 1;
                    GlobalVariables.playerY = Console.WindowHeight / 2; //Center of the screen
                }
            }
        }
    }
}